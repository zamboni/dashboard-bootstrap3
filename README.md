# Dashboard with Bootstrap3


This dashboard is intended to use as a base for bootstrap3 templates.

## Requirements

To use it, add to installed apps:

```python
INSTALLED_APPS = [
    'dashboard_bootstrap3'
]
```
